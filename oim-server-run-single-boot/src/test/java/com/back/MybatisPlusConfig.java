package com.back;

import java.io.IOException;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.baomidou.mybatisplus.MybatisConfiguration;
import com.baomidou.mybatisplus.MybatisXMLLanguageDriver;
import com.baomidou.mybatisplus.entity.GlobalConfiguration;
import com.baomidou.mybatisplus.mapper.LogicSqlInjector;
import com.baomidou.mybatisplus.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.plugins.PerformanceInterceptor;
import com.baomidou.mybatisplus.spring.MybatisSqlSessionFactoryBean;
import com.onlyxiahui.query.mybatis.dao.GenericDAO;
import com.onlyxiahui.query.mybatis.dao.JdbcTemplate;
import com.startup.DataSourceConfig;
import com.startup.myplus.OIMObjectHandler;

@Configuration
@MapperScan("dao.manage.mybatis.mysql.mapper")
public class MybatisPlusConfig {

	@Autowired
	DataSourceConfig dataSourceConfig;

	// //////////////////////////////////////////写数据源

//	@Bean(name = "txManager")
//	public DataSourceTransactionManager txManager() {
//		DataSourceTransactionManager bean = new DataSourceTransactionManager();
//		bean.setDataSource(dataSourceConfig.writeDataSource());
//		return bean;
//	}

	@Bean(name = "writeSqlSessionFactory")
	public SqlSessionFactory writeSqlSessionFactory() {
		SqlSessionFactory factory = null;
		MybatisSqlSessionFactoryBean bean = new MybatisSqlSessionFactoryBean();
		bean.setDataSource(dataSourceConfig.writeDataSource());
		bean.setTypeAliasesPackage("com.im.server.general.manage.common.bean");
		MybatisConfiguration configuration = new MybatisConfiguration();
		configuration.setDefaultScriptingLanguage(MybatisXMLLanguageDriver.class);
		configuration.setJdbcTypeForNull(JdbcType.NULL);
		
		configuration.addMappers("/");
		bean.setConfiguration(configuration);
		bean.setPlugins(new Interceptor[] {
				new PaginationInterceptor(),
				new PerformanceInterceptor(),
				new OptimisticLockerInterceptor()
		});
		bean.setGlobalConfig(globalConfiguration());

		//
		//bean.setConfigLocation(new ClassPathResource("mybatis-config.xml"));
		try {
			factory = bean.getObject();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return factory;
	}

	// @Bean(name = "writeSqlSessionFactory")
	// public SqlSessionFactoryBean writeSqlSessionFactory() {
	// PathMatchingResourcePatternResolver resolver = new
	// PathMatchingResourcePatternResolver();
	// SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
	// try {
	// bean.setDataSource(dataSourceConfig.writeDataSource());
	// bean.setConfigLocation(new ClassPathResource("mybatis-config.xml"));
	// bean.setMapperLocations(resolver.getResources("classpath*:/dao/manage/mapper/mysql/*.xml"));
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// return bean;
	// }

	@Bean(name = "writeSqlSessionTemplate")
	public SqlSessionTemplate writeSqlSessionTemplate() {
		SqlSessionTemplate bean = null;
		try {
			bean = new SqlSessionTemplate(writeSqlSessionFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bean;
	}

	@Bean(name = "writeGenericDAO")
	public GenericDAO writeGenericDAO() {
		GenericDAO bean = new GenericDAO();
		bean.setSqlSessionTemplate(writeSqlSessionTemplate());
		return bean;
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////
	
	
	
	/**
	 * 读数据源
	 *
	 * @return
	 */
//	@Bean(name = "readSqlSessionFactory")
//	public SqlSessionFactory readSqlSessionFactory() {
//		SqlSessionFactory factory = null;
//		MybatisSqlSessionFactoryBean bean = new MybatisSqlSessionFactoryBean();
//		bean.setDataSource(dataSourceConfig.readDataSource());
//		bean.setTypeAliasesPackage("com.im.server.general.manage.common.bean");
//		MybatisConfiguration configuration = new MybatisConfiguration();
//		configuration.setDefaultScriptingLanguage(MybatisXMLLanguageDriver.class);
//		configuration.setJdbcTypeForNull(JdbcType.NULL);
//		bean.setConfiguration(configuration);
//		bean.setPlugins(new Interceptor[] {
//				new PaginationInterceptor(),
//				new PerformanceInterceptor(),
//				new OptimisticLockerInterceptor()
//		});
//		bean.setGlobalConfig(globalConfiguration());
//
//		//
//		bean.setConfigLocation(new ClassPathResource("mybatis-config.xml"));
//		try {
//			factory = bean.getObject();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return factory;
//	}
	
	
	@Bean(name = "readSqlSessionFactory")
	public SqlSessionFactoryBean readSqlSessionFactory() {
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();

		try {
			bean.setDataSource(dataSourceConfig.readDataSource());
			bean.setConfigLocation(new ClassPathResource("mybatis-config.xml"));
			bean.setMapperLocations(resolver.getResources("classpath*:/dao/manage/mybatis/mysql/*/*.xml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bean;
	}

	@Bean(name = "readSqlSessionTemplate")
	public SqlSessionTemplate readSqlSessionTemplate() {
		SqlSessionTemplate bean = null;
		try {
			bean = new SqlSessionTemplate(readSqlSessionFactory().getObject());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bean;
	}

	@Bean(name = "readGenericDAO")
	public GenericDAO readGenericDAO() {
		GenericDAO bean = new GenericDAO();
		bean.setSqlSessionTemplate(readSqlSessionTemplate());
		return bean;
	}

	@Bean(name = "jdbcTemplate")
	public JdbcTemplate jdbcTemplate() {
		JdbcTemplate bean = new JdbcTemplate();
		bean.setDataSource(dataSourceConfig.writeDataSource());
		return bean;
	}

	@Bean
	public GlobalConfiguration globalConfiguration() {
		GlobalConfiguration conf = new GlobalConfiguration(new LogicSqlInjector());
		conf.setLogicDeleteValue("-1");
		conf.setLogicNotDeleteValue("1");
		conf.setIdType(2);
		conf.setMetaObjectHandler(new OIMObjectHandler());
		return conf;
	}
}
