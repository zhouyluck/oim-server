package com.startup.myplus;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public class DynamicDataSource extends AbstractRoutingDataSource {
	@Override
	protected Object determineCurrentLookupKey() {
		DatabaseType type = DatabaseContextHolder.getDatabaseType();
		if (type == null) {
			logger.info("========= dataSource ==========" + DatabaseType.slave.name());
			return DatabaseType.slave.name();
		}
		logger.info("========= dataSource ==========" + type);
		return type;
	}
}
