package com.startup.myplus;

public enum  DatabaseType {
	
	master("write"),slave("read");

	private DatabaseType(String name) {
		this.name = name();
	}

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
