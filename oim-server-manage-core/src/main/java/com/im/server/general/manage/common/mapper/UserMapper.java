package com.im.server.general.manage.common.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.im.server.general.manage.common.bean.User;

public interface UserMapper extends BaseMapper<User> {

}
