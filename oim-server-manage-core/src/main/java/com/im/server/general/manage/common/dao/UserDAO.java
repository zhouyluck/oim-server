package com.im.server.general.manage.common.dao;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.im.server.general.manage.common.bean.User;
import com.im.server.general.manage.common.mapper.UserMapper;

/**
 * @author: XiaHui
 * @date: 2018-04-26 14:05:18
 */
@Repository
public class UserDAO extends BaseDAO {
	
	@Resource
	protected UserMapper  userMapper;
	
	String namespace = User.class.getName();

	public User getUserByAccount(String account) {
		User user = this.readDAO.queryForObject(namespace + ".getUserByAccount", account, User.class);
		return user;
	}

	public <T> List<T> queryList(Map<String, Object> map, Class<T> clazz) {
		List<T> list = this.readDAO.queryForList(namespace + ".queryList", map, clazz);
		return list;
	}
	
	public void add(User user){
		user.setId(this.getId());
		userMapper.insert(user);
	}
}
